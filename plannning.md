# Planning

## Research

-   [ ] Vocab
-   [ ] Functions
-   [ ] Methods

## Problem decomposition

-   [ ] Input
-   [ ] Output
-   [ ] Examples
-   [ ] Conditions (if)
-   [ ] Iteration (loop)

## Problems

### 01 minimum_value

-function receives two parameters (int, int)
-compare the two parameters to see which is less - if v1 is larger than v2 -- return v2 - otherwise return v1

### 04 max_of_three

function receives three parameters
create placeholder for max value
compare v1 to v2 -> larger value is assigned as max
compare v3 to max value -> larger value is assigned as max
return max value

### 06 can_skydive

function receives two parameters (int, bool)
both have to be true for return to be true

### 09 is_palindrome

function receives word with no spaces as a parameter (str)
create var and assign to reverse of the orig string
if the orig string is equal to the reverse of the string -- return true
otherwise return false

### 10 is_divisible_by_3

function receives a number as a parameter (int)
if the number mod 3 is equal to zero, return 'fizz'
otherwise, just return the number

### 11 is_divisible_by_5

function receives a number as a parameter (int)
if the number mod 5 is equal to zero, return 'buzz'
otherwise, just return the number

### 12 fizzbuzz

function receives a number as a parameter (int)
first check if the number is divisible by both 3 and 5 if so, return 'fizzbuzz'
otherwise, if it is divisible by 3 return fizz
otherwise, if it is divisible by 5 return buzz

### 14 can_make_pasta

function receives single parameter as a list of 3 ingredients
list must have exactly 3 items
normalize list to match case of reference
the list must contain and only contain 'flour', 'eggs', and 'oil'
if so, return true
else return false

### 16 is_inside_bounds

receives two parameters x and y (int, int)
check that both x and y are between 0 and 10, inclusive - return true if so
otherwise, return false

### 19 is_inside_bounds

receives 6 parameters (x, y, rect_x, rect_y, rect_width, rect_height)
range of x is (rect_x, rect_x+rect_width+1)
range of y is (rect_y, rect_y+rect_height+1)
check that both x and y are within those ranges

### 20 has_quorum

receives two lists (attendees_list, members_list)
compare attendees list to half of the member list
if greater or equal to, return true
if not, return false

### 22 gear_for_day

receives two bool params (is_workday, is_sunny)
start with empty list of gear
if not sunny, and workday, append umbrella to gear list
if is workday, append laptop to gear list
if not workday, append surfboard

-test cases -
true, true gear = ['laptop']
true, false gear = ['laptop', umbrella]
false, true gear = ['surfboard']
false, false gear = ['surfboard']

### 24 calculate_average

receives a list of values
check if list is empty - return None if so.
add up all values in the list
divide total in list by the length of the list
return value rounded to the nearest 2 decimal places

-test cases-
[1,2,3,4,5,6,7] == 4.00
[1,3,7] == 3.67
[] == None

### 25 calculate_sum

receives list of values
if list is empty, return None
loop through list and accumulate the total
return total

-test cases-
[1,2,3,4,5,6,7] == 28
[1,-3,7] == 5
[] == None

### 26 calculate_grade

receives list of grades
if list is empty, return None
compute average of grades using problem 24
check if average is greater than 90 - isA
check if average is greater than 80 - isB
check if average is greater than 70 - isC
check if average is greater than 60 - isD
otherwise isF

-test cases-
[50, 79, 30, 100, 90, 90]

### 27 calculate_max

receives a list of values
if list is empty, return None
declare max value variable
loop through values list
if value is larger than max, reassign max
return max

### 28 remove_duplicate_letters

receives a string as a parameter
turn string into list of chars
declare variable to hold cleaned up list
loop through string - if current char does not exist in new_list, append to new_list
return joined list to string

### 30 find_second_largest

receives a list of values
if len(list) <= 1, return none
sort list
second largest value in list will be at index len(list) - 2

### 31 sum_of_sqaures

recieves a list of values
if list is empty, return none
initialize var for acc
loop through list and acc the square of the value

### 32 sum_of first_n_numbers

receives a parameter limit
for loop from zero up to and including limit
accumulate the sum
return the sum

### 33 sum_of_first_n_even_numbers

receives a parameter limit
for loop from zero up to and including limit ignoring odd numbers
accumulate the sum
return the sum

### 34 count_letters_and_digits(s)

receives a string as a param that could contain letters or numbers
declare vars for letters counter
declare var for numbers counter
loop through string and increment letters or numbers
return letters, numbers

### 35 count_letters_and_digits(s)

skipping the pseudo code

### 37 pad_left

receives 3 paramters (int, int, str)
turn number into string
if length of number is greater than the param length, return the number
while len(number) is less than length, keep adding on a pad to the front
return the result
