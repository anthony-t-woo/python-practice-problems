# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    result = []
    if len(list) % 2 == 0:
        for i in range(int(len(list)/2)):
            result.append(list[i])
    else:
        for i in range(int(len(list)/2) + 1):
            result.append(list[i])
    return result

print(halve_the_list([1, 2, 3, 4, 5]))