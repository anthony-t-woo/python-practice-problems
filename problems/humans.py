class Human:
    def __init__(self, name):
        self.name = name
        self.spells = {}

    def greet(self):
        print("Hello, my name is " + self.name) 


class Mage(Human):
    def learn_spell(self, spell_name, effect):
        self.spells.update({spell_name: effect})
        print(self.name + " learned " + spell_name) 

    def cast_spell(self, spell_name):
        if spell_name in self.spells:
            print(self.name + " cast " + spell_name + ". "
                  + self.spells.get(spell_name))
        else:
            print(self.name + " does not know that one. Try learning it first")


human = Mage("Bob")
human.learn_spell("flame_ball", "hot fire flame")
human.cast_spell("flame_ball")
human.cast_spell("ice")

