# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    if len(password) > 12 or len(password) < 6:
        return False
    is_upper = 0
    is_lower = 0
    is_spec = 0
    is_dig = 0
    special_chars = ["$", "!", "@"]
    for char in password:
        if char.isupper():
            is_upper += 1
        elif char.islower():
            is_lower += 1
        elif char.isdigit():
            is_dig += 1
        elif char in special_chars:
            is_spec += 1
    if is_upper > 0 and is_lower > 0 and is_spec > 0 and is_dig > 0:
        return True
    else:
        return False
    

print(check_password('1aaL54'))    