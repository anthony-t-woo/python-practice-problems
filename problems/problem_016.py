# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.
# x >= 0 and x <= 10 and y >= 0 and y <= 10:
def is_inside_bounds(x, y):
    if x in range(0, 11) and y in range(0, 11):
        return True
    else:
        return False
