from problems import problem_001, problem_004, problem_006, problem_009, \
                    problem_010, problem_011, problem_012, problem_014, \
                    problem_016, problem_019, problem_020, problem_022, \
                    problem_024, problem_025, problem_026, problem_027, \
                    problem_028, problem_030, problem_031, problem_032, \
                    problem_033, problem_034, problem_035, problem_037

if problem_001.minimum_value(4, 5) == 4:
    print('P-1 Pass')
else:
    print('P-1 Fail')

if problem_001.minimum_value(6, -14) == -14:
    print('P-1 Pass')
else:
    print('P-1 Fail')

if problem_004.max_of_three(-30, 25, -40) == 25:
    print('P-4 Pass')
else:
    print('P-4 Fail')

if problem_006.can_skydive(25, True) is True:
    print('P-6.1 Pass')
else:
    print('P-6.1 Fail')

if problem_006.can_skydive(12, True) is False:
    print('P-6.2 Pass')
else:
    print('P-6.2 Fail')

if problem_009.is_palindrome('racecar'):
    print('P-9.1 Pass - racecar is a palindrome')
else:
    print('P-9.1 Fail - racecar should have been a palindrome')

if problem_009.is_palindrome('notapalindrome'):
    print('P-9.2 Fail - notapalindrome is not a palindrome')
else:
    print('P-9.2 Pass - notapalindrome is not a palindrome')

if problem_010.is_divisible_by_3(22) == 22:
    print('P-10.1 Pass')
else:
    print('P-10.1 Fail')

if problem_010.is_divisible_by_3(21) == 'fizz':
    print('P-10.1 Pass')
else:
    print('P-10.1 Fail')

if problem_011.is_divisible_by_5(41) == 41:
    print('P-11.1 Pass')
else:
    print('P-11.1 Fail')

if problem_011.is_divisible_by_5(20) == 'buzz':
    print('P-11.1 Pass')
else:
    print('P-11.1 Fail')

if problem_012.fizzbuzz(14) == 14:
    print('P-12.1 Pass')
else:
    print('P-12.1 Fail')

if problem_012.fizzbuzz(15) == 'fizzbuzz':
    print('P-12.2 Pass')
else:
    print('P-12.2 Fail')

if problem_012.fizzbuzz(9) == "fizz":
    print('P-12.3 Pass')
else:
    print('P-12.3 Fail')

if problem_012.fizzbuzz(50) == "buzz":
    print('P-12.4 Pass')
else:
    print('P-12.4 Fail')

if problem_014.can_make_pasta(["not", "in", "pasta"]):
    print('P-14.1 Fail - should not be able to make pasta')
else:
    print('P-14.1 Pass')

if problem_014.can_make_pasta(["flour", "eggs", "oil"]):
    print('P-14.2 Pass')
else:
    print('P-14.2 Fail - should have been able to make pasta')

if problem_014.can_make_pasta(["flour", "eggs"]):
    print('P-14.3 Fail - should not be able to make pasta')
else:
    print('P-14.3 Pass')

if problem_016.is_inside_bounds(16, 4):
    print('P-16.1 Fail')
else:
    print('P-16.1 Pass')

if problem_016.is_inside_bounds(-5, 3):
    print('P-16.2 Fail')
else:
    print('P-16.2 Pass')

if problem_016.is_inside_bounds(4, 3):
    print('P-16.3 Pass')
else:
    print('P-16.3 Fail')

if problem_016.is_inside_bounds(10, 10):
    print('P-16.4 Pass')
else:
    print('P-16.4 Fail')

if problem_016.is_inside_bounds(0, 0):
    print('P-16.5 Pass')
else:
    print('P-16.5 Fail')

if problem_019.is_inside_bounds(3, 6, 1, 6, 3, 3):
    print('P-19.1 Pass')
else:
    print('P-19.1 Fail')

if not problem_019.is_inside_bounds(3, 6, 4, 6, 3, 3):
    print('P-19.1 Pass')
else:
    print('P-19.1 Fail')

att = ['k', 'd', 'a', 'f', 'k', 'l', 'd']
mem = ['k', 'd', 'a', 'k', 'f', 'j', 'a', 'e', 'k']
if problem_020.has_quorum(att, mem):
    print('P-20.1 Pass')
else:
    print('P-20.1 Fail')

att = ['k', 'd']
mem = ['k', 'd', 'a', 'k', 'f', 'j', 'a', 'e', 'k']
if not problem_020.has_quorum(att, mem):
    print('P-20.2 Pass')
else:
    print('P-20.1 Fail')

if problem_022.gear_for_day(True, True) == ['laptop']:
    print('P-22.1 Pass')
else:
    print('P-22.1 Fail')

if problem_022.gear_for_day(True, False) == ['umbrella', 'laptop']:
    print('P-22.2 Pass')
else:
    print('P-22.2 Fail')

if problem_022.gear_for_day(False, True) == ['surfboard']:
    print('P-22.3 Pass')
else:
    print('P-22.3 Fail')

if problem_022.gear_for_day(False, False) == ['surfboard']:
    print('P-22.4 Pass')
else:
    print('P-22.4 Fail')

if problem_024.calculate_average([1, 2, 3, 4, 5, 6, 7]) == 4.00:
    print('P-24.1 Pass')
else:
    print('P-24.1 Fail')

if problem_024.calculate_average([1, 3, 7]) == 3.67:
    print('P-24.2 Pass')
else:
    print('P-24.2 Fail')

if problem_024.calculate_average([]) is None:
    print('P-24.3 Pass')
else:
    print('P-24.3 Fail')

if problem_025.calculate_sum([1, 2, 3, 4, 5, 6, 7]) == 28:
    print('P-25.1 Pass')
else:
    print('P-25.1 Fail')

if problem_025.calculate_sum([1, -3, 7]) == 5:
    print('P-25.2 Pass')
else:
    print('P-25.2 Fail')

if problem_025.calculate_sum([]) is None:
    print('P-25.3 Pass')
else:
    print('P-25.3 Fail')

print(problem_026.calculate_grade([50, 79, 30, 100, 90, 90]), "should be C")
print(problem_026.calculate_grade([80, 79, 100, 60, 85, 85]), "should be B")

print(problem_027.max_in_list([50, 79, 30, 100, 90, 90]), "should be 100")
print(problem_027.max_in_list([50, 100, 90, 901, 5003]), "should be 5003")
print(problem_027.max_in_list([]), "should be None")

print(problem_028.remove_duplicate_letters('jdfckeakeakdkceka'),
      "should be jdfckea")

if problem_030.find_second_largest([50, 79, 30, 100, 90, 90]) == 90:
    print('P-30.1 Pass')
else:
    print('P-30.1 Fail')

if problem_030.find_second_largest([50, 100, 909, 901, 5003]) == 909:
    print('P-30.2 Pass')
else:
    print('P-30.2 Fail')

if problem_030.find_second_largest([]) is None:
    print('P-30.3 Pass')
else:
    print('P-30.3 Fail')

if problem_031.sum_of_squares([]) is None:
    print('P-31.1 Pass')
else:
    print('P-31.1 Fail')

if problem_031.sum_of_squares([1, 2, 3]) == 14:
    print('P-31.2 Pass')
else:
    print('P-31.2 Fail')

if problem_031.sum_of_squares([-1, 0, 1]) == 2:
    print('P-31.3 Pass')
else:
    print('P-31.3 Fail')

if problem_032.sum_of_first_n_numbers(-1) is None:
    print('P-32.1 Pass')
else:
    print('P-32.1 Fail')

if problem_032.sum_of_first_n_numbers(1) == 1:
    print('P-32.2 Pass')
else:
    print('P-32.2 Fail')

if problem_032.sum_of_first_n_numbers(2) == 3:
    print('P-32.3 Pass')
else:
    print('P-32.3 Fail')

if problem_032.sum_of_first_n_numbers(5) == 15:
    print('P-32.4 Pass')
else:
    print('P-32.4 Fail')

if problem_033.sum_of_first_n_even_numbers(-1) is None:
    print('P-33.1 Pass')
else:
    print('P-33.1 Fail')

if problem_033.sum_of_first_n_even_numbers(0) == 0:
    print('P-33.2 Pass')
else:
    print('P-33.2 Fail')

if problem_033.sum_of_first_n_even_numbers(1) == 2:
    print('P-33.3 Pass')
else:
    print('P-33.3 Fail')

if problem_033.sum_of_first_n_even_numbers(2) == 6:
    print('P-33.4 Pass')
else:
    print('P-33.4 Fail')

if problem_033.sum_of_first_n_even_numbers(5) == 30:
    print('P-33.5 Pass')
else:
    print('P-33.5 Fail')

if problem_034.count_letters_and_digits("") == (0, 0):
    print('P-34.1 Pass')
else:
    print('P-34.1 Fail')

if problem_034.count_letters_and_digits("a") == (1, 0):
    print('P-34.2 Pass')
else:
    print('P-34.2 Fail')

if problem_034.count_letters_and_digits("1") == (0, 1):
    print('P-34.3 Pass')
else:
    print('P-34.3 Fail')

if problem_034.count_letters_and_digits("1a") == (1, 1):
    print('P-34.4 Pass')
else:
    print('P-34.4 Fail')

if problem_035.count_letters_and_digits("") == (0, 0):
    print('P-35.1 Pass')
else:
    print('P-35.1 Fail')

if problem_035.count_letters_and_digits("a") == (1, 0):
    print('P-35.2 Pass')
else:
    print('P-35.2 Fail')

if problem_035.count_letters_and_digits("1") == (0, 1):
    print('P-35.3 Pass')
else:
    print('P-35.3 Fail')

if problem_035.count_letters_and_digits("1a") == (1, 1):
    print('P-35.4 Pass')
else:
    print('P-35.4 Fail')

if problem_037.pad_left(34, 8, "*") == '******34':
    print('P-37.1 Pass')
else:
    print('P-37.1 Fail')

if problem_037.pad_left(15, 1, "*") == '15':
    print('P-37.2 Pass')
else:
    print('P-37.2 Fail')

if problem_037.pad_left(1000, 10, "0") == '0000001000':
    print('P-37.3 Pass')
else:
    print('P-37.3 Fail')
